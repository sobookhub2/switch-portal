function debugLog(s, message) {
	const debug = s.getPropertyValue("Debug") === "Yes"
	if (debug) {
		s.log(1, message);
	}
}

function forEach(array, callback) {
	var currentValue, index;
	for (i = 0 ; i < array.length; i ++) {
	 if (typeof array[i] === undefined) {
		currentValue = null;
	 } else {	
		currentValue = array[i];
	 }
	 index = i;
	 callback(currentValue, i, array);
	}
}

function normalizeChannelProperty(s, value) {
	return value.replace(/[^a-zA-Z0-9\-]/gi, "");
}

function getChannelKey(s, flow, scope, channel, programId) {
	const prefix = {
		portal: "Portals_",
		channel: "C-",
		flow: "F-",
		global: "G-",
		program: "P-"
	};
	const segmentSeperator = "_";
	var channelKey = prefix.portal;
	if (scope === "Global") {
		channelKey += prefix.channel + channel;
	} else if(scope === "Program") {
		channelKey += prefix.program + programId + segmentSeperator + prefix.channel + channel;
	} else {
		channelKey += prefix.flow + flow + segmentSeperator + prefix.channel + channel;
	}
	return channelKey;
}

function getDirectorySeperator(s) {
	return s.isMac() ? "/" : "\\";
}

function initDirectory(s, pathArray, key) {
	cwd = "";
	forEach(pathArray, function(pathSegment, i) {
		cwd += pathSegment;
		dir = new Dir(cwd);
		if(!dir.exists) {
			dir.cdUp();
			debugLog(s, "initDirectory: '" + dir.absPath + "' '"+pathSegment+"' did not exist. Creating it.");
			try {
				dir.mkdir(pathSegment);
			} catch (e) {
				s.log(3, "Could not make '" + key + "'. " + e);
			}
		}
		cwd += getDirectorySeperator(s);
	})
	lastDir = new Dir(cwd);
	return lastDir.absPath;
}

// Packing function
function packJob(s, job, tempFolder, destinationPath, flow, scope, channel, programId) {
	var packLocation = initDirectory(s, [tempFolder, job.getUniqueNamePrefix()], "pack location");
	var datasetDir = initDirectory(s, [packLocation, "datasets"], "dataset location", true);
	
	//function
	var writeDatasets = function() {
		 var datasetTags = job.getDatasetTags();
		 if (datasetTags.length > 0) {
			debugLog(s, datasetTags.length + " external datasets found.");
		 }
		// Replace with forEach
		forEach(datasetTags, function(tag) {
			debugLog(s, "Packing dataset: "+ tag);
			dataset = job.getDataset(tag);
			if (dataset) {
				datasetPath = dataset.getPath();
				datasetModel = dataset.getModel();
				datasetDestination = datasetDir + getDirectorySeperator(s) + tag + "." + datasetModel;
				datasetCopyResult = s.copy(datasetPath, datasetDestination);
				if(!datasetCopyResult) {
					s.log(3, "Could not copy dataset to temporary location: " + datasetDestination);
				}
			} else {
				s.log(2, "Dataset '"+ tag + "' could not be retreived. Skipping.");
			}
		});
	};

	var writeJobTicket = function() {
		// XML helper function
		var createElement = function(parent, doc, key, value, attributeAsKey) {
			if(typeof(attributeAsKey) == "undefined") {
				attributeAsKey = false;
			}
			var parentLength, index, child;
			parentLength = parent.getChildNodes().length;
			index = "Key"+parentLength;
			if(attributeAsKey == true) {
				child = doc.createElement(index, null);
			} else {
				child = doc.createElement(key, null);
			}
			// Set (sometimes) redundant key
			child.addAttribute("key", null, key);
			parent.appendChild(child);
			if(value) {
				text = doc.createText( value );
				child.appendChild(text);
			}
			return child;
		}

		var getPrivateData = function(parent, doc) {
				var privateDataTags = job.getPrivateDataTags();
			 // Make parent PD node
			 var pdParent =  createElement(parent, doc, "getPrivateData", null);
				if(privateDataTags.length > 0) {
				debugLog(s, privateDataTags.length + " private data tags found.")
				// Replace with forEach
				forEach(privateDataTags, function(tag) {
					// Create it
					createElement(pdParent, doc, tag, job.getPrivateData(tag), true);
				});
				}
				return true;
		 }

		// Write XML job ticket
		var doc = new Document();
		var root = doc.createElement("JobTicket");
		doc.setDocumentElement(root);
		createElement(root, doc, "getHierarchyPath", job.getHierarchyPath());
		createElement(root, doc, "getEmailAddresses", job.getEmailAddresses());
		createElement(root, doc, "getEmailBody", job.getEmailBody());
		createElement(root, doc, "getJobState", job.getJobState());
		createElement(root, doc, "getUserName", job.getUserName());
		createElement(root, doc, "getUserFullName", job.getUserFullName());
		createElement(root, doc, "getUserEmail", job.getUserEmail());
		createElement(root, doc, "getPriority", job.getPriority());
		createElement(root, doc, "getArrivalStamp", job.getArrivalStamp());
		createElement(root, doc, "getName", job.getName());
		createElement(root, doc, "getUniqueNamePrefix", job.getUniqueNamePrefix());
		createElement(root, doc, "getExtension", job.getExtension());
		// Do private data
		getPrivateData(root, doc);
		// Save some Portal-specific stuff
		var portals = doc.createElement("SourcePortal");
		doc.setDocumentElement(root);
		createElement(portals, doc, "getFlowName", s.getFlowName());
		createElement(portals, doc, "getChannelKey", getChannelKey(s, flow, scope, channel, programId));
		createElement(portals, doc, "getScope", scope);
		createElement(portals, doc, "getChannel", channel);
		createElement(portals, doc, "getProgramId", programId);
		createElement(portals, doc, "getElementName", s.getElementName() );
		createElement(portals, doc, "getPortalInDate", new Date().toString() );
		// Save XML as file
		var jobTicketPath = job.createPathWithName("ticket.xml");
		doc.save(jobTicketPath);
		debugLog(s, "Job ticket written to: " + jobTicketPath);
		// Move job ticket
		var ticketPath = initDirectory(s, [packLocation, "jobTicket"], "job ticket location", true);
		var ticketDestination = ticketPath + getDirectorySeperator(s) +  "ticket.xml";
		var ticketCopyResult = s.copy(jobTicketPath, ticketDestination);
		if(!ticketCopyResult) {
			s.log(3, "Could not copy job ticket to temporary location: " + ticketDestination);
		}
	};

	var archiveJob = function() {
		 // Copy job to temp directory
		 var jobTempPath = "";
		// Folders
		if(job.isFolder()) {
			jobTempFolder = job.createPathWithName("contents", true);
			contentsDir = new Dir(jobTempFolder);
			try {
				contentsDir.mkdir(job.getName());
			} catch(e) {
				s.log(3, "Could not mkdir 'contents' in '"+ contentsDir +"' " + e);
			}
			jobTempPath = jobTempFolder + getDirectorySeperator(s) + job.getName();
		// Files
		} else {
			contentsDir = initDirectory(s, [packLocation, "contents"], "contents location", true);
			jobTempPath = contentsDir + getDirectorySeperator(s) + job.getName();
			jobTempFolder = contentsDir;
		}
		var copyResult = s.copy(job.getPath(), jobTempPath);
		debugLog(s, "job.getPath(): " + job.getPath());
		debugLog(s, "jobTempPath: " + jobTempPath);
		debugLog(s, "copyResult: " + copyResult);
		if(!copyResult) {
			s.log(3, "Job failed to copy to the temporary archive location.");
			return false;
		}
		// Archive
		var password = "";
		var compress = false;
		var removeExisting = false;
		var packDestination = destinationPath + ".zip";
		var assetArchiveSuccess = s.archive(jobTempFolder, packDestination, password, compress, removeExisting);
		var datasetArchiveSuccess = s.archive(packLocation + getDirectorySeperator(s) + "datasets", packDestination, password, compress, removeExisting);
		var ticketArchiveSuccess = s.archive(packLocation + getDirectorySeperator(s) + "jobTicket", packDestination, password, compress, removeExisting);

		debugLog(s, "Archive saved to: " + packDestination);
		debugLog(s, "assetArchiveSuccess: " + assetArchiveSuccess +
				 	" datasetArchiveSuccess: "+ datasetArchiveSuccess +
				 	" ticketArchiveSuccess: " + ticketArchiveSuccess);
		 return assetArchiveSuccess;
	};

	// Do it
	writeDatasets();
	writeJobTicket();
	var archiveResult = archiveJob();
	
	if(!archiveResult) {
		return false;
	} else {
		return packLocation;
	}
};

function jobArrived( s : Switch, job : Job ) {
	var channel = normalizeChannelProperty(s, s.getPropertyValue("Channel"));
	var debug = s.getPropertyValue("Debug");
	var scope = normalizeChannelProperty(s, s.getPropertyValue("Scope"));
	var programId = normalizeChannelProperty(s, s.getPropertyValue("ProgramId"));
	var flow = s.getFlowName();
	var etherName = "SwitchPortalsEther";
	var scriptDataFolder = s.getSpecialFolderPath("ScriptData");
	var completeFilename = "_" + job.getUniqueNamePrefix() + "_" + job.getName();
	var channelKey = getChannelKey(s, flow, scope, channel, programId);
	var etherPath = initDirectory(s, [scriptDataFolder, etherName], "ether folder");
	var channelFolder = initDirectory(s, [etherPath, channelKey], "channel folder");
	var tempFolder = initDirectory(s, [channelFolder, "temp"], "temp channel folder");
	var destinationPath = channelFolder +  getDirectorySeperator(s) + completeFilename;
	// Debugging
	debugLog(s, "etherPath: " + etherPath);
	debugLog(s, "scriptDataFolder: " + scriptDataFolder);
	debugLog(s, "scope: " + scope);
	debugLog(s, "completeFilename: " + completeFilename);
	debugLog(s, "channelKey: " + channelKey);
	debugLog(s, "channelFolder: " + channelFolder);
	debugLog(s, "tempFolder: " + tempFolder);
	debugLog(s, "completeFilename: " + completeFilename);
	debugLog(s, "destinationPath: " + destinationPath);

	// Pack the job and move it
	var tempPackLocation = packJob(s, job, tempFolder, destinationPath, flow, scope, channel, programId);
	
	if(tempPackLocation) {
		// Remove the temporarily packed job
		var tempPackDir = new Dir(tempPackLocation);
		try {
			tempPackDir.rmdirs();
		} catch (e) {
			s.log(3, "Could not delete the temporay packed job. " + e);
		}
		// Remove this job from Switch
		job.sendToNull(job.getPath());
	}
	return;
}